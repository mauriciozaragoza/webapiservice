Ext.require(['Ext.data.*', 'Ext.grid.*']);

Ext.define('Customer', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'Id',
        type: 'int',
        useNull: true
    }, 'Name', 'Surname'],
    validations: [{
        type: 'length',
        field: 'Name',
        min: 1
    },
	{
        type: 'length',
        field: 'Surname',
        min: 1
    }]
});

Ext.onReady(function(){

    var store = Ext.create('Ext.data.Store', {
        autoLoad: true,
        autoSync: true,
        model: 'Customer',
        proxy: {
            type: 'rest',
            url: '/api/customer',
			// headers: { 
				// 'Content-Type': 'application/json; charset=utf-8; Cache-Control: no-cache'
			// },
			appendId: true,
			pageParam: false, //to remove param "page"
            startParam: false, //to remove param "start"
            limitParam: false, //to remove param "limit"
            noCache: false, //to remove param "_dc"			
            reader: {
                type: 'json',
				root: ''
            },
            writer: {
                type: 'json'
            }
        },
        listeners: {
            write: function(store, operation){
                var record = operation.getRecords()[0],
                    name = Ext.String.capitalize(operation.action),
                    verb;
                    
                if (name == 'Destroy') {
                    record = operation.records[0];
                    verb = 'Destroyed';
                } else {
                    verb = name + 'd';
                }
                Ext.example.msg(name, Ext.String.format("{0} user: {1}", verb, record.getId()));
            }
        }
    });
    
    var rowEditing = Ext.create('Ext.grid.plugin.RowEditing');
    
    var grid = Ext.create('Ext.grid.Panel', {
        renderTo: document.body,
        plugins: [rowEditing],
        width: 400,
        height: 300,
        frame: true,
        title: 'Users',
        store: store,
        iconCls: 'icon-user',
        columns: [{
            text: 'ID',
            width: 40,
            sortable: true,
            dataIndex: 'Id'
        }, {
            header: 'Name',
            width: 80,
            sortable: true,
            dataIndex: 'Name',
            field: {
                xtype: 'textfield'
            }
        }, {
            header: 'Surname',
            width: 80,
            sortable: true,
            dataIndex: 'Surname',
            field: {
                xtype: 'textfield'
            }
        }],
        dockedItems: [{
            xtype: 'toolbar',
            items: [{
                text: 'Add',
                iconCls: 'icon-add',
                handler: function(){
                    // empty record
                    store.insert(0, new Customer());
                    rowEditing.startEdit(0, 0);
                }
            }, '-', {
                itemId: 'delete',
                text: 'Delete',
                iconCls: 'icon-delete',
                disabled: true,
                handler: function(){
                    var selection = grid.getView().getSelectionModel().getSelection()[0];
                    if (selection) {
                        store.remove(selection);
                    }
                }
            }]
        }]
    });
    grid.getSelectionModel().on('selectionchange', function(selModel, selections){
        grid.down('#delete').setDisabled(selections.length === 0);
    });
});
