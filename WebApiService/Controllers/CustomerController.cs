﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiService.Models;

namespace WebApiService.Controllers
{
    public class CustomerController : ApiController
    {
        List<Customer> data;

        public CustomerController()
        {
            data = new List<Customer>
            {
                new Customer
                {
                    Id = 1,
                    Name = "Jose",
                    Surname= "Lizarraga"
                },
                new Customer
                {
                    Id = 2,
                    Name = "Saul",
                    Surname= "Hernandez"
                },
                new Customer
                {
                    Id = 3,
                    Name = "Carlos",
                    Surname= "Alvarez"
                },
            };
        }

        // GET api/customer
        public IEnumerable<Customer> Get()
        {
            return data;
        }

        // GET api/customer/5
        public Customer Get(int id)
        {
            return (from v in data
                    where v.Id == id
                    select v).Single();
        }

        // POST api/customer
        public IEnumerable <Customer>  Post(Customer value)
        {
            data.Add(value);
            return data;
        }

        // PUT api/customer/5
        public void Put(int id, string value)
        {
        }

        // DELETE api/customer/5
        public IEnumerable<Customer> Delete(int id)
        {
            data.Remove((from v in data
                    where v.Id == id
                    select v).Single());
            return data;
        }
    }
}
